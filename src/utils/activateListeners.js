import { changeScreen } from 'store/siteActions'
import { dispatcher } from 'store'

const mediaMatches = {
  500: window.matchMedia('(max-width:500px)'),
  501: window.matchMedia('(min-width:501px)'),
  800: window.matchMedia('(max-width:800px)'),
  801: window.matchMedia('(min-width:801px)'),
  1920: window.matchMedia('(min-width:1280px)'),
}

const listeners = {}

function handleResize(width) {
  return (evt) => {
    if (evt.matches) {
      dispatcher(changeScreen(width))
    }
  }
}

export default function useActivateListeners() {
  Object.keys(mediaMatches).forEach((med) => {
    listeners[med] = handleResize(med)
    mediaMatches[med].addListener(listeners[med])
  })
  handleResize(document.body.scrollWidth)({ matches: true })
}
