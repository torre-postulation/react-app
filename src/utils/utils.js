import { validate } from 'validate.js'

export function changeTitle(title) {
  const newDocumentTitle = `Torre - ${title}`
  if (document.title !== newDocumentTitle) {
    document.title = newDocumentTitle
  }
}

/**
 * generate Random number by limit
 *
 * @export
 * @param {number} [maxLimit=9999]
 * @returns
 */
export function randomNumber(maxLimit = 9999) {
  return Math.round(Math.random() * maxLimit)
}

/**
 * Generate Random keys for iteration
 *
 * @export
 * @param {string} [prefix=`random-key`]
 * @param {*} [params=[]]
 */
export function generateRandomKey(prefix = `random-key`, param = ``) {
  return `${prefix}-${randomNumber()}-${param}`
}

/**
 * Get Forms values from fieldsbyid
 *
 * @export
 * @param {*} fieldsById
 * @returns
 */
export function getFormsValues(fieldsById) {
  const formValues = {}
  // get values by id and name
  Object.keys(fieldsById).forEach((id) => {
    const { value } = fieldsById[id]
    if (value) {
      if ((Array.isArray(value) && value.length > 0) || !Array.isArray(value)) {
        if (Array.isArray(value)) {
          formValues[id] = formValues[id] ? [...formValues[id], ...value] : value
        } else {
          formValues[id] = value
        }
      }
    }
  })
  return formValues
}

/**
 * Returns required rules on form fields
 *
 * @export
 * @param {*} fields
 * @returns
 */
export function getFormsRules(fields, values) {
  const formRules = {}
  Object.keys(fields).forEach((id) => {
    const { rules = {} } = fields[id]
    formRules[id] = rules
  })
  return validate(values, formRules)
}

export function setFormRules(field) {}
