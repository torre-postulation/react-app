export const types = {
  LOGIN: 'LOGING_USER',
}

const initialState = {
  user: null,
}

export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.LOGIN: {
      return {
        ...state,
        user: action.meta,
      }
    }
    default:
      return state
  }
}

export default userReducer
