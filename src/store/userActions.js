import { types } from './userReducer'

export const setUser = (user) => {
  return {
    type: types.LOGIN,
    meta: user,
  }
}
