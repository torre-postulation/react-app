import { createStore, combineReducers, applyMiddleware } from 'redux'
import { createPromise } from 'redux-promise-middleware'
import reduxInmutableStateInvariant from 'redux-immutable-state-invariant'
// app reducers
import userReducer from './userReducer'
import siteReducer from './siteReducer'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__

export const suffixes = {
  START: '_START',
  DONE: '_DONE',
  ERROR: '_ERROR',
}

const middlewares = [
  reduxInmutableStateInvariant(),
  createPromise({
    promiseTypeSuffixes: Object.keys(suffixes),
  }),
]

const store = createStore(
  combineReducers({ userReducer, siteReducer }),
  {},
  (composeEnhancers && composeEnhancers(applyMiddleware(...middlewares))) ||
    applyMiddleware(...middlewares),
)

export const dispatcher = store.dispatch
export default store
