import { types } from './siteReducer'

export const changeScreen = (screenSize) => ({
  type: types.CHANGE_SCREEN,
  meta: screenSize,
})

export const changeRoutesGroup = (routeKind) => ({
  type: types.CHANGE_ROUTE,
  meta: routeKind,
})

export const setUtils = (utils) => ({
  type: types.SET_UTILS,
  meta: utils,
})

export const changeLanguage = (language) => {
  localStorage.setItem('user-language', language)
  return {
    type: types.CHANGE_LANGUAGE,
    meta: language,
  }
}

export const changeAccentColor = (colorHex) => {
  document.documentElement.style.setProperty('--user-color', colorHex)
  return {
    type: types.CHANGE_ACCENT_COLOR,
    meta: colorHex,
  }
}
