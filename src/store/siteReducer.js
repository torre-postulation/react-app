export const types = {
  CHANGE_SCREEN: 'CHANGE_SCREEN',
  CHANGE_ROUTE: 'CHANGE_ROUTE',
  CHANGE_LANGUAGE: 'CHANGE_LANGUAGE',
  CHANGE_ACCENT_COLOR: 'CHANGE_ACCENT_COLOR',
  SET_UTILS: 'SET_UTILS',
}

const initalState = {
  screen: 1920,
  routeGroup: 'public',
  language: localStorage.getItem('user-language') ? localStorage.getItem('user-language') : 'eng',
  color: '#cddc39',
  deafultSearch: 'person',
}

export const siteReducer = (state = initalState, action) => {
  switch (action.type) {
    case types.CHANGE_SCREEN: {
      return {
        ...state,
        screen: action.meta,
      }
    }
    case types.CHANGE_ROUTE: {
      return {
        ...state,
        routeGroup: action.meta,
      }
    }
    case types.CHANGE_ACCENT_COLOR: {
      return {
        ...state,
        color: action.meta,
      }
    }
    case types.CHANGE_LANGUAGE: {
      return {
        ...state,
        language: action.meta,
      }
    }
    case types.SET_UTILS: {
      localStorage.setItem('user-language', action.meta.language)
      document.documentElement.style.setProperty('--user-color', action.meta.strong_color)
      return {
        ...state,
        language: action.meta.language,
        color: action.meta.strong_color,
        deafultSearch: action.meta.default_search_kind,
      }
    }
    default:
      return state
  }
}

export default siteReducer
