import React from 'react'

const LandingPage = React.lazy(() => import(`views/home`))
const SearchView = React.lazy(() => import(`views/search`))
const Login = React.lazy(() => import(`views/login`))
const Register = React.lazy(() => import(`views/register`))
const SearchSelect = React.lazy(() => import(`views/searchSelect`))
const ColorSelect = React.lazy(() => import(`views/colorSelect`))
const Profile = React.lazy(() => import(`views/profile`))
const Gnoma = React.lazy(() => import(`views/gnoma`))

const routes = {
  public: [
    {
      path: '/sign-up',
      key: 'register',
      component: Register,
      exact: true,
      useLayout: false,
      title: 'Sign Up',
    },
    {
      path: '/',
      key: 'login',
      component: Login,
      useLayout: false,
      title: 'Login',
      exact: true,
    },
  ],
  user: [
    {
      path: '/',
      key: 'homepage',
      component: LandingPage,
      exact: true,
      title: 'Home',
    },
    {
      path: '/select-search',
      key: 'select search',
      component: SearchSelect,
      title: 'Select Search Kind',
      useLayout: false,
      exact: true,
    },
    {
      path: '/select-color',
      key: 'select color',
      component: ColorSelect,
      title: 'Select Accent Color',
      useLayout: false,
      exact: true,
    },
    {
      path: '/my-profile',
      key: 'my profile',
      component: Profile,
      title: 'My Profile',
      exact: true,
    },
    {
      path: '/person/:username',
      key: 'username',
      component: Gnoma,
      title: 'Talent Profile',
      exact: true,
    },
    {
      path: ['/search/:kind/:text', '/search/:kind/:text/:page'],
      key: 'search',
      component: SearchView,
      exact: true,
    },
  ],
}

export default routes
