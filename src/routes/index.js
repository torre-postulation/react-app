import React, { Suspense } from 'react'
import { Switch, Route } from 'react-router-dom'
import NotFound from 'views/notFound'
import Routes from './routes'
import { Loader } from 'components'
import { useSelector } from 'react-redux'

export default function Router() {
  const { routeGroup } = useSelector(({ siteReducer }) => siteReducer)
  return (
    <Suspense fallback={<Loader fullScreen />}>
      <Switch>
        {Routes[routeGroup].map((routeProps) => (
          <Route {...routeProps} />
        ))}
        <Route component={NotFound} />
      </Switch>
    </Suspense>
  )
}
