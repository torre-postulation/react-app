import React from 'react'
import css from './styles.module.css'

export default function Button({ color = 'black', label, onClick = {} }) {
  return (
    <button className={`${css.button} ${css[color]} `} onClick={onClick}>
      {label}
    </button>
  )
}
