import React from 'react'
import css from './css.module.css'

const colors = [
  '#850A0A',
  '#0A2B85',
  '#850A64',
  '#0A852B',
  '#85750A',
  '#FFFFFF',
  '#EC7979',
  '#438DB4',
  '#CF51AD',
  '#55B74A',
  '#CDDC39',
  '#8F8F8F',
]

export default function ColorsSelect({ value, onChange, min = false, id = 'ColorsSelect' }) {
  return (
    <div className={`full flex ${css.colorContainer}`}>
      {colors.map((hex) => {
        return (
          <div
            key={hex}
            className={`${value === hex && css.selected} ${css.colorDot}`}
            style={{
              borderColor: hex,
              width: min ? '30px' : '150px',
              height: min ? '30px' : '150px',
              margin: min ? '5px' : '30px',
              padding: min ? '3px' : '10px',
            }}
            onClick={() => onChange(hex, id)}
          >
            <div
              className={css.dot}
              style={{
                background: hex,
              }}
            />
          </div>
        )
      })}
    </div>
  )
}
