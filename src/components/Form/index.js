import React, { useEffect } from 'react'
import classes from './style.module.css'
import { generateRandomKey, getFormsValues, getFormsRules } from 'utils'
import { useMergedState } from 'hooks'
import { Input } from 'components'
import Button from 'components/Button'

export default function Form({
  fieldsById,
  sections,
  dispatcher = () => {},
  submit: { acceptButton = null, cancelButton = null, styles = { justifyContent: 'center' } },
  onSubmit = () => {},
  onCancel = () => {},
}) {
  const [{ fields, structure }, setState] = useMergedState({
    fields: {},
    structure: [],
  })

  useEffect(() => {
    setState({ fields: { ...fieldsById }, structure: sections })
    // eslint-disable-next-line
  }, [fieldsById, sections])

  function setInputValue(event) {
    const { value, id } = event
    setState({
      fields: {
        ...fields,
        [id]: {
          ...fields[id],
          value,
        },
      },
    })
  }

  function submit() {
    const filledForms = getFormsValues(fields)
    const formRulesMessages = getFormsRules(fields, filledForms)
    if (formRulesMessages) {
      const newFields = {}
      Object.keys(formRulesMessages).forEach((id) => {
        newFields[id] = {
          ...fields[id],
          error: formRulesMessages[id][0],
        }
      })
      return setState({ fields: { ...fields, ...newFields } })
    }
    return onSubmit(filledForms)
  }

  function makeRequest({ requestData, fieldId }) {
    const {
      request,
      types: { start, success },
    } = requestData
    dispatcher({ type: start, meta: fieldId })
    request.send().then(({ data }) => dispatcher({ type: success, payload: data, meta: fieldId }))
  }

  function callDidMountRequest({ fieldId, didMount: requestData }) {
    makeRequest({ requestData, fieldId })
  }

  return (
    <div className={`flex full ${classes.wrapper} flex-wrap`}>
      <div className={classes.regularSection}>
        {structure?.fieldsIds?.length > 0 &&
          structure.fieldsIds.map((fieldId, index) => {
            return (
              <Input
                {...fields[fieldId]}
                id={fieldId}
                fieldsById={fields}
                didMountDispatcher={callDidMountRequest}
                valueListener={setInputValue}
                key={generateRandomKey(`input-${fieldId}-${index}`)}
              />
            )
          })}
        {(acceptButton || cancelButton) && (
          <div className="flex full padd-top-20" style={{ ...styles }}>
            {acceptButton && <Button {...acceptButton} onClick={submit} />}
            {cancelButton && <Button {...cancelButton} onClick={onCancel} />}
          </div>
        )}
      </div>
    </div>
  )
}
