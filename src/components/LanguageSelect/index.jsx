import React from 'react'
import css from './css.module.css'

export default function LanguageSelect({ value, onChange = () => {}, id = 'LanguageSelect' }) {
  return (
    <div className={`flex ${css.container}`}>
      <div
        className={`${value === 'eng' && css.selected} ${css.lang}`}
        onClick={() => onChange('eng', 'LanguageSelect')}
      >
        Eng
      </div>
      <div
        className={`${value === 'esp' && css.selected} ${css.lang}`}
        onClick={() => onChange('esp', id)}
      >
        Esp
      </div>
    </div>
  )
}
