import React from 'react'
import css from './Loader.module.css'
import { ReactComponent as Torre } from 'assets/icons/torre.svg'

export default function Loader({ fullScreen = false }) {
  function loader() {
    return (
      <div className={css.loader}>
        <div className={css.gooey}>
          <span className={css.dot} />
          <div className={css.dots}>
            <span />
            <span />
            <span />
          </div>
        </div>
      </div>
    )
  }
  if (fullScreen) {
    return (
      <div className={css.wrapper}>
        <div className={css.logoCont}>
          <Torre />
          {loader()}
        </div>
      </div>
    )
  }
  return loader()
}
