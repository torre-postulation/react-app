import React from 'react'
import css from './css.module.css'
import { ReactComponent as Arrow } from 'assets/icons/arrow.svg'
import { useHistory, useRouteMatch } from 'react-router-dom'

export default function Pagination({ offset, size, total, page }) {
  const { push } = useHistory()
  const { params, url } = useRouteMatch()
  const shownext = total / size > 0
  const pageNum = Number(page)

  function goNext() {
    if (params.page) {
      const urlparts = url.split('/')
      push(`/${urlparts[1]}/${urlparts[2]}/${urlparts[3]}/${pageNum + 1}`)
    } else {
      push(`${url}/${pageNum + 1}`)
    }
  }

  function goPrev() {
    if (params.page) {
      const urlparts = url.split('/')
      push(`/${urlparts[1]}/${urlparts[2]}/${urlparts[3]}/${pageNum - 1}`)
    } else {
      push(`${url}/${pageNum - 1}`)
    }
  }

  return (
    <div className={`flex center ${css.wrapper}`}>
      {offset > 1 && <Arrow className={css.left} onClick={goPrev} />}
      {`Mostrando Resultados del ${offset + 1} al ${total + 1}`}
      {shownext && <Arrow className={css.rigth} onClick={goNext} />}
    </div>
  )
}
