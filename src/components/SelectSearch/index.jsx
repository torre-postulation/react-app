import React from 'react'
import css from './css.module.css'
import { ReactComponent as People } from 'assets/icons/people.svg'
import { ReactComponent as Case } from 'assets/icons/case.svg'

export default function SelectSearch({ value, onChange, min, id = 'SelectSearch' }) {
  return (
    <div className={`flex full ${!min && css.icons}`}>
      <div
        className={`${min ? css.minContainer : css.iconContainer} ${
          value === 'person' && css.selected
        }`}
        onClick={() => onChange('person', 'SelectSearch')}
      >
        <People />
      </div>
      <div
        className={`${min ? css.minContainer : css.iconContainer} ${
          value === 'job' && css.selected
        }`}
        onClick={() => onChange('job', id)}
      >
        <Case />
      </div>
    </div>
  )
}
