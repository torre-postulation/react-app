import React from 'react'
import { Input } from 'components'
import { generateRandomKey } from 'utils'
import classes from './styles.module.css'

export default function InputsGroup({
  fieldsIds,
  fieldsById,
  label,
  alignItems = `flex-start`,
  ...rest
}) {
  const elementsWidth = Math.ceil(100 / fieldsIds.length - 2)
  return (
    <div className={'between flex full flex-wrap'} style={{ alignItems }}>
      {label && <h1 className={classes.label}>{label}</h1>}
      {fieldsIds
        .filter((id) => id !== ``)
        .map((fieldId, index) => {
          return (
            <div
              className={`full flex center ${classes.inputGroup}`}
              style={{ width: `${elementsWidth}%` }}
              key={generateRandomKey(`input-group-${index}`)}
            >
              <Input {...rest} {...fieldsById[fieldId]} fieldsById={fieldsById} id={fieldId} />
            </div>
          )
        })}
    </div>
  )
}
