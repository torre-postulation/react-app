import React from 'react'
import css from './styles.module.css'
import { ReactComponent as Email } from 'assets/icons/email.svg'
import { ReactComponent as Person } from 'assets/icons/person.svg'
import { ReactComponent as Security } from 'assets/icons/security.svg'
import { ReactComponent as Globe } from 'assets/icons/globe.svg'
import { ReactComponent as ColorPalette } from 'assets/icons/color.svg'
import { ReactComponent as Search } from 'assets/icons/search.svg'
import { ReactComponent as Top } from 'assets/icons/top.svg'
import { ReactComponent as List } from 'assets/icons/list.svg'

export default function InputIcon({ icon }) {
  function renderIcon() {
    switch (icon) {
      case 'email':
        return <Email className={css.inputIcon} />
      case 'person':
        return <Person className={css.inputIcon} />
      case 'security':
        return <Security className={css.inputIcon} />
      case 'globe':
        return <Globe className={css.inputIcon} />
      case 'colors':
        return <ColorPalette className={css.inputIcon} />
      case 'search':
        return <Search className={css.inputIcon} />
      case 'top':
        return <Top className={css.inputIcon} />
      case 'count':
        return <List className={css.inputIcon} />
      default:
        return <Email className={css.inputIcon} />
    }
  }

  return <div className={`flex center ${css.iconContainer}`}>{renderIcon()}</div>
}
