import React, { Fragment, useEffect } from 'react'
import InputsGroup from './InputsGroup'
import TextInput from './TextInput'

export default function Input({
  type = `text`,
  hidden = false,
  didMount,
  disabled,
  didMountDispatcher,
  valueListener,
  loading = false,
  ...rest
}) {
  const props = {
    ...rest,
    loading,
    didMount,
    disabled,
    valueListener,
    didMountDispatcher,
    type,
    ...(type !== `group` && {
      onChange: valueListener,
      onBlur: valueListener,
    }),
  }
  function checkDimount() {
    if (!hidden && didMount && !loading) {
      didMountDispatcher({ fieldId: props.id, didMount })
    }
  }

  // eslint-disable-next-line
  useEffect(checkDimount, [hidden, disabled])

  function renderInput() {
    switch (type.toLowerCase()) {
      case `text`:
      case `number`:
      case `password`:
        return <TextInput {...props} />
      case `group`:
        return <InputsGroup {...props} />
      default:
        return null
    }
  }

  return <Fragment>{!hidden && renderInput()}</Fragment>
}
