import React, { useEffect, useState } from 'react'
import classes from './styles.module.css'
import InputWrapper from './InputWrapper'

export default function TextInputWrapper({
  id,
  onBlur,
  onChange,
  value,
  type = `text`,
  inputType = `text`,
  disabled = false,
  placeholder = ``,
  viewMode = false,
  ...rest
}) {
  const [innerVal, saveVal] = useState('')

  useEffect(() => {
    saveVal(value)
  }, [value])

  function saveLocalValue({ target: { value } }) {
    saveVal(value)
  }

  function handleNewValue() {
    if (!viewMode) {
      return onBlur({
        name: id,
        value: innerVal,
        id,
      })
    }
  }

  return (
    <InputWrapper type={inputType} disabled={disabled || viewMode} {...rest}>
      <input
        placeholder={placeholder}
        id={id}
        name={id}
        value={innerVal}
        type={type}
        disabled={viewMode || disabled}
        className={`${classes.textInput} ${viewMode && classes.viewMode}`}
        onChange={saveLocalValue}
        onBlur={handleNewValue}
      />
    </InputWrapper>
  )
}
