import React from 'react'
import classes from './styles.module.css'
import InputIcon from './InputIcon'

export default function InputWrapper({
  children,
  error = false, //Puede ser boolean o string
  label,
  subLabel = ``,
  disabled = false,
  positionLabel = `left`,
  borderTop = false,
  footer = ``,
  withPadding = false,
  checkbox = false,
  icon = null,
  rules: { presence = false } = {},
}) {
  return (
    <div
      className={`${classes.inputWrapper} full flex ${borderTop && classes.borderTop} ${
        withPadding ? classes.extraPadding : ``
      }`}
    >
      <div
        className={`flex full ${classes.container} ${
          positionLabel === `right` && classes.positionLabelWrapper
        } ${disabled && classes.disabled}`}
      >
        {label && (
          <div className={`flex center ${checkbox ? classes.checkLabel : classes.regularLabel}`}>
            <h3 className={`${classes.label}`}>{label}</h3>
            {presence && <p className={classes.requiredDot}>*</p>}
          </div>
        )}
        <div
          className={`flex between wrap full ${classes.inputContainer} ${
            checkbox
              ? classes.checkboxInput
              : positionLabel === `right`
              ? classes.positionLabel
              : ``
          } ${error && classes.inputError}`}
        >
          {subLabel && <h3 className={`full ${classes.text} ${classes.subLabel}`}>{subLabel}</h3>}
          <div className="full flex">
            {icon && <InputIcon icon={icon} />}
            {children}
          </div>
          {footer && <h3 className={`full ${classes.text} ${classes.subLabel}`}>{footer}</h3>}
        </div>
        {error && <h3 className={`full ${classes.error}`}>{error}</h3>}
      </div>
    </div>
  )
}
