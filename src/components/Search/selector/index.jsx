import React, { useEffect } from 'react'
import { useMergedState } from 'hooks'
import css from './styles.module.css'
import { ReactComponent as People } from 'assets/icons/people.svg'
import { useSelector } from 'react-redux'
import { ReactComponent as Case } from 'assets/icons/case.svg'

export default function IconSelector({ onChange }) {
  const { deafultSearch } = useSelector(({ siteReducer }) => siteReducer)
  const [{ selector }, setState] = useMergedState({
    selector: deafultSearch,
  })

  useEffect(() => {
    onChange(selector)
  }, [selector, onChange])

  return (
    <div className={css.wrapper}>
      {selector === 'person' && (
        <div
          className={`${css.iconContainer} ${css.people} ${css.selected}`}
          onClick={() => setState({ selector: 'person' })}
        >
          <People />
        </div>
      )}
      {selector === 'case' && (
        <div
          className={`${css.iconContainer} ${css.selected}`}
          onClick={() => setState({ selector: 'people' })}
        >
          <Case />
        </div>
      )}
    </div>
  )
}
