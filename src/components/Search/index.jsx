import React, { Fragment } from 'react'
import { ReactComponent as Search } from 'assets/icons/search.svg'
import css from './styles.module.css'
import IconSelector from './selector'
import { useMergedState } from 'hooks'
import Request from 'api'
import { useHistory } from 'react-router-dom'

export default function SearchBar({ headMode = false, searchMode = '' }) {
  const { push } = useHistory()
  const [{ open, selector }, setState] = useMergedState({
    open: false,
    selector: searchMode,
    value: '',
  })

  function handleChange({ code, target: { value } }) {
    setState({ value })
    if (code === 'Enter' || code === 'NumpadEnter') {
      const encodedText = encodeURI(value)
      const kind = searchMode ? searchMode : selector
      push(kind === 'person' ? `/search/people/${encodedText}` : `/search/jobs/${encodedText}`)
      const req = new Request('newSearch', {
        data: {
          search_string: value,
          kind,
        },
      })
      req.send()
      setState({ open: false, value: '' })
    }
  }

  if (headMode) {
    return (
      <div className={`flex between ${css.wrapper} ${css.head} ${open && css.animation}`}>
        {!open && <Search className={css.icon} onClick={() => setState({ open: !open })} />}
        {open && (
          <Fragment>
            <input
              placeholder="Busca"
              className={`${css.headMo} ${css.input}`}
              type="text"
              onKeyDown={handleChange}
            />
            <IconSelector onChange={(selector) => setState({ selector })} />
          </Fragment>
        )}
      </div>
    )
  }

  return (
    <div className={`flex center ${css.wrapper} full`}>
      <Search className={css.search} />
      <input
        placeholder="Busca por personas o trabajos"
        className={css.input}
        type="text"
        onKeyDown={handleChange}
      />
    </div>
  )
}
