import { useState } from 'react'

/**
 * Essencial hook to handle nested state, optimize component lifecycle
 *
 * @export
 * @param {*} [initialState={}]
 * @returns [State, Dispatcher]
 */
export default function useMergedState(initialState = {}) {
  // component state
  const [nestedState, setNestedState] = useState(initialState)

  // component use state dispatcher
  function stateDispatcher(newState = {}, forwardCallback = undefined) {
    const nextState = (oldState => {
      const nextState = { ...oldState, ...newState }
      return nextState
    })(nestedState)
    setNestedState(nextState)
    if (forwardCallback) forwardCallback(nextState)
  }
  return [nestedState, stateDispatcher]
}
