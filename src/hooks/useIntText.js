import { useSelector } from 'react-redux'
import languageMock from 'lang'

export default function useIntText(section) {
  const { language } = useSelector(({ siteReducer }) => siteReducer)
  return languageMock[section][language]
}
