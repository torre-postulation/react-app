const form = ({ password, email, acceptButton }) => ({
  fieldsById: {
    password: {
      type: `password`,
      icon: 'security',
      ...password,
    },
    email: {
      icon: 'email',
      type: `text`,
      ...email,
    },
  },
  sections: {
    fieldsIds: ['email', 'password'],
  },
  submit: {
    acceptButton,
    styles: {
      justifyContent: 'flex-end',
      paddingTop: '20px',
    },
  },
})
export default form
