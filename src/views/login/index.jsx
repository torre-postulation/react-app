import React, { useState } from 'react'
import css from './css.module.css'
import login from 'assets/img/login.png'
import { ReactComponent as Torre } from 'assets/icons/torre.svg'
import { useIntText } from 'hooks'
import formData from './form'
import { Form, Loader } from 'components'
import { Link } from 'react-router-dom'
import Request from 'api'
import { useDispatch } from 'react-redux'
import { setUser } from 'store/userActions'
import { setUtils } from 'store/siteActions'

export default function Login({ history: { push } }) {
  const { first, strong, link, form } = useIntText('login')
  const [loading, setLoading] = useState(false)
  const data = formData(form)
  const dispatch = useDispatch()

  function handleSubmit(data) {
    if (!loading) {
      setLoading(true)
      const request = new Request('login', { data })
      request
        .send()
        .finally(() => {
          setLoading(false)
        })
        .then(({ data: { user, token } }) => {
          localStorage.setItem('user', token)
          Request.authToken = token
          dispatch(setUser(user))
          dispatch(setUtils(user.utils))
          push('/')
        })
        .catch((message) => console.log(message))
    }
  }

  return (
    <div className={`flex full ${css.wrapper}`}>
      <div className={`flex ${css.imageWrapper}`}>
        <img src={login} alt="login" className={css.image} />
      </div>
      <div className={css.container}>
        <Torre className={css.torre} />
        <span className={`flex full center`}>
          <h1 className={css.title}>{first}</h1>
          <h1 className={css.strong}>{strong}</h1>
        </span>
        <Form {...data} onSubmit={handleSubmit} />
        {loading && <Loader />}
        <Link to="/sign-up">
          <p className={css.link}>{link}</p>
        </Link>
      </div>
    </div>
  )
}
