import { Button, Loader } from 'components'
import { useIntText, useMergedState } from 'hooks'
import React, { Fragment, useEffect } from 'react'
import { ReactComponent as Edit } from 'assets/icons/edit.svg'
import { ReactComponent as Person } from 'assets/icons/person.svg'
import css from './css.module.css'
import Request from 'api'
import InputIcon from 'components/Input/InputIcon'
import { ColorsSelect, SelectSearch, LanguageSelect } from 'components'
import { useDispatch } from 'react-redux'
import { setUser } from 'store/userActions'
import { setUtils } from 'store/siteActions'
import { Link } from 'react-router-dom'

function TopSearchs({ value }) {
  const { topSearchMessage } = useIntText('profile')
  if (value.length > 0) {
    return (
      <div className="full flex wrap pad-top-10">
        {value.map(({ id, search_string, kind }) => {
          const encodedText = encodeURI(search_string)
          return (
            <Link
              key={id}
              className={css.linkCont}
              to={
                kind === 'person' ? `/search/people/${encodedText}` : `/search/jobs/${encodedText}`
              }
            >
              {search_string}
            </Link>
          )
        })}
      </div>
    )
  }
  return (
    <div className="pad-top-10">
      <p>{topSearchMessage}</p>
    </div>
  )
}

function SearchsCount({ value: { person_search, jobs_search, searchs_count } }) {
  const { searches, personSearch, jobsSearch } = useIntText('profile')
  return (
    <div className={`flex between ${css.countCont} pad-top-10`}>
      <div className="flex">
        <p className={css.count}>{searchs_count}</p>
        <p className={css.countLabel}>{searches}</p>
      </div>
      <div className="flex">
        <p className={css.count}>{person_search}</p>
        <p className={css.countLabel}>{personSearch}</p>
      </div>
      <div className="flex">
        <p className={css.count}>{jobs_search}</p>
        <p className={css.countLabel}>{jobsSearch}</p>
      </div>
    </div>
  )
}

const inputComponents = ({ id, value, onChange = () => {} }) => {
  const components = {
    strong_color: <ColorsSelect min value={value} onChange={onChange} id={id} />,
    default_search_kind: <SelectSearch min value={value} onChange={onChange} id={id} />,
    language: <LanguageSelect value={value} onChange={onChange} id={id} />,
    topSearch: <TopSearchs value={value} />,
    searchCount: <SearchsCount value={value} />,
  }
  return components[id]
}

export default function Profile() {
  const [{ loading, editing, data, originalData }, setState] = useMergedState({
    loading: true,
    data: {},
    originalData: {},
    editing: false,
  })
  const {
    personalData,
    userPreferences,
    dataForm,
    searchs,
    preferencesForm,
    save,
    searchsForm,
    cancel,
  } = useIntText('profile')
  const dispatcher = useDispatch()

  function saveChanges() {
    setState({ loading: true })
    const req = new Request('updateProfile', {
      data: {
        name: data.name,
        email: data.email,
        password: data.password,
        language: data.utils.language,
        default_search_kind: data.utils.default_search_kind,
        strong_color: data.utils.strong_color,
      },
    })
    req.send().finally(() => requestProfile(true))
  }

  function setInput(value, form) {
    if (form === 'language' || form === 'default_search_kind' || form === 'strong_color') {
      setState({
        editing: true,
        data: {
          ...data,
          utils: {
            ...data.utils,
            [form]: value,
          },
        },
      })
    } else {
      setState({
        data: {
          ...data,
          [form]: value,
        },
      })
    }
  }

  function requestProfile(updateUserRedux = false) {
    const req = new Request('profile')
    req.send().then(({ data }) => {
      if (updateUserRedux) {
        const { utils, ...rest } = data
        dispatcher(setUser(rest))
        dispatcher(setUtils(utils))
      }
      setState({ data, originalData: data, loading: false, editing: false })
    })
  }
  // eslint-disable-next-line
  useEffect(requestProfile, [])

  return (
    <div className={css.wrapper}>
      <div className={css.container}>
        {loading ? (
          <Loader />
        ) : (
          <Fragment>
            <div className="full flex wrap between">
              <div className={`flex center wrap ${css.imageContainer}`}>
                <div className={css.image}>
                  <Person className={css.imageIcon} />
                </div>
                <h2 className={css.name}>{originalData.name}</h2>
                <h3 className={css.email}>{originalData.email}</h3>
              </div>
              <div className={`${css.paper} flex between wrap ${css.personalData}`}>
                <div className={css.personalCont}>
                  <span className="flex full wrap">
                    <h1 className={css.strong}>{personalData.strong}</h1>
                    <h1 className={css.title}>{personalData.second}</h1>
                    <div className={css.line} />
                  </span>
                  {dataForm(data).map(({ id, label, icon, value }) => {
                    return (
                      <div className={css.formInputCont} key={id}>
                        <div className="full flex start">
                          <InputIcon icon={icon} />
                          <p className={css.inputLabel}>{label}</p>
                          {!editing && (
                            <Edit
                              className={css.edit}
                              onClick={() => setState({ editing: true })}
                            />
                          )}
                        </div>
                        {editing ? (
                          <input
                            className={css.input}
                            type="text"
                            value={value}
                            onChange={({ target: { value } }) => setInput(value, id)}
                          />
                        ) : (
                          <p className={css.inputValue}>{value}</p>
                        )}
                      </div>
                    )
                  })}
                  {editing && (
                    <div className="flex full between pad-top-20">
                      <Button label={save} onClick={saveChanges} color="min" />
                      <Button
                        color="min"
                        label={cancel}
                        onClick={() => setState({ editing: false, data: originalData })}
                      />
                    </div>
                  )}
                </div>
                <div className={css.personalCont}>
                  <span className="flex full wrap">
                    <h1 className={css.strong}>{userPreferences.strong}</h1>
                    <h1 className={css.title}>{userPreferences.second}</h1>
                    <div className={css.line} />
                  </span>
                  {preferencesForm(data).map(({ id, label, icon, value }) => {
                    return (
                      <div className={css.formInputCont} key={id}>
                        <div className="full flex start">
                          <InputIcon icon={icon} />
                          <p className={css.inputLabel}>{label}</p>
                        </div>
                        {inputComponents({ id, value, onChange: setInput })}
                      </div>
                    )
                  })}
                </div>
              </div>
            </div>
            <div className={`${css.paper} full ${css.searchs}`}>
              <span className="flex full wrap">
                <h1 className={css.strong}>{searchs.strong}</h1>
                <h1 className={css.title}>{searchs.second}</h1>
                <div className={css.line} />
              </span>
              {searchsForm(data).map(({ id, label, icon, value }) => {
                return (
                  <div className={css.formInputCont} key={id}>
                    <div className="full flex start">
                      <InputIcon icon={icon} />
                      <p className={css.inputLabel}>{label}</p>
                    </div>
                    {inputComponents({ id, value })}
                  </div>
                )
              })}
            </div>
          </Fragment>
        )}
      </div>
    </div>
  )
}
