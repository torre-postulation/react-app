import React, { useEffect } from 'react'
import Request from 'api'
import { useMergedState } from 'hooks'
import Loader from 'components/Loader'
import css from './styles.module.css'
import JobCard from './JobCard'
import PeopleCard from './PeopleCard'
import { Pagination } from 'components'

const params = {
  currency: 'USD$',
  page: 0,
  periodicity: 'hourly',
  lang: 'es',
  size: 20,
  aggregate: true,
  offset: 0,
}

export default function SearchView({
  match: {
    params: { kind = '', text = '', page = 0 },
  },
  history: { push },
}) {
  const [{ results, loading, pagination }, setState] = useMergedState({
    results: [],
    loading: true,
    pagination: {},
  })

  function makeRequest() {
    if (!loading) {
      setState({ loading: true })
    }
    let baseUrl = ''
    if (kind !== 'people' && kind !== 'jobs') {
      push('/not-found')
    }
    if (kind === 'people') {
      baseUrl += 'people/_search/'
    }
    if (kind === 'jobs') {
      baseUrl += 'opportunities/_search/'
    }
    const request = new Request('search', {
      url: baseUrl,
      params: {
        ...params,
        page: page > 0 ? Number(page) - 1 : 0,
        offset: page > 0 ? (Number(page) - 1) * params.size : 20,
      },
      data: {
        ...(kind === 'people' && {
          name: {
            term: text,
          },
        }),
        ...(kind === 'jobs' && {
          'skill/role': {
            experience: 'potential-to-develop',
            text,
          },
        }),
      },
    })
    request.send().then(({ offset, results, size, total }) => {
      if (results.length) {
        setState({ results, pagination: { offset, size, total, page }, loading: false })
      }
    })
  }

  // eslint-disable-next-line
  useEffect(makeRequest, [kind, text, page])

  return (
    <div className={`full flex ${css.wrapper} center`}>
      <div className={`full flex ${css.container} center`}>
        {loading && <Loader />}
        {results.length > 0 && !loading && (
          <div>
            {kind === 'people'
              ? results.map((data, index) => {
                  return <PeopleCard {...data} key={`people-search-result-${index}`} />
                })
              : results.map((data, index) => {
                  return <JobCard {...data} key={`job-search-result-${index}`} />
                })}
            <Pagination {...pagination} />
          </div>
        )}
      </div>
    </div>
  )
}
