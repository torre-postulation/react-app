import React from 'react'
import css from './css.module.css'
import { ReactComponent as Case } from 'assets/icons/case.svg'
import { Link } from 'react-router-dom'

function convertToK(amount) {
  if (amount > 1000) {
    return `${amount / 1000}K`
  }
  return `${amount}`
}

const parses = {
  periodicity: {
    monthly: 'por Mes',
    yearly: 'Por año',
  },
}

export default function PeopleCard({
  compensations,
  locationName,
  name,
  picture,
  professionalHeadline,
  username,
  skills,
}) {
  return (
    <div className={css.wrapper}>
      <div className={css.imageContainer}>
        {picture && <img src={picture} alt="user profile" />}
      </div>
      <div className={css.container}>
        <h2 className={css.title}>
          <Link to={`/person/${username}`}>{name}</Link>
        </h2>
        <h2 className={css.location}>
          <Link to={`/person/${username}`}>{`@${username}`}</Link>
        </h2>
        <h2 className={css.enterprise}>{professionalHeadline}</h2>
        {locationName && <h2 className={css.location}>{locationName}</h2>}
        {skills.length > 0 && (
          <div className={`flex bewteen wrap ${css.skillsWrap}`}>
            {skills.map(({ name }, index) => (
              <span className={css.skillCont} key={`${name}-${index}`}>
                <p className={css.skill}>{name}</p>
              </span>
            ))}
          </div>
        )}
      </div>
      <div className={css.chartsContainer}>
        {compensations?.employee && (
          <div className={css.moneyCont}>
            <Case className={css.case} />
            <p className={css.currency}>{compensations.employee.currency}</p>
            <p className={css.payment}>{`${convertToK(compensations.employee.amount)}`}</p>
            <p className={css.period}>{parses.periodicity[compensations.employee.periodicity]}</p>
          </div>
        )}
      </div>
    </div>
  )
}
