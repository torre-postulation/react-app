import React from 'react'
import css from './css.module.css'
import { ReactComponent as Case } from 'assets/icons/case.svg'
import { ReactComponent as Money } from 'assets/icons/money.svg'

const parses = {
  types: {
    'full-time-employment': 'Tiempo Completo',
    'freelance-gigs': 'Empleo para Freelance',
  },
  periodicity: {
    monthly: 'por Mes',
    yearly: 'Por año',
  },
}

function convertToK(amount) {
  return `${amount / 1000}K`
}

export default function JobCard({
  id,
  objective,
  type,
  organizations,
  locations,
  remote,
  compensation,
  skills,
  members,
}) {
  const location = locations?.map((loc, ind) => (ind > 0 ? `${loc}, ` : loc))

  return (
    <div className={css.wrapper}>
      <div className={css.imageContainer}>
        {organizations?.length > 0 && <img src={organizations[0].picture} alt="company priofile" />}
      </div>
      <div className={css.container}>
        <h2 className={css.title}>{objective}</h2>
        {organizations?.length > 0 && <h2 className={css.enterprise}>{organizations[0].name}</h2>}
        <h2 className={css.location}>{location}</h2>
        <span className={`flex center  ${css.workCont}`}>
          <Case className={css.case} />
          <p className={css.work}>{`${remote ? 'Remoto' : 'Presencial'}, ${parses.types[type]}`}</p>
        </span>
        {members?.length > 0 && (
          <span className={`flex center ${css.membContainer}`}>
            <p className={css.memb}>Miembros:</p>
            <div className={`flex center ${css.members}`}>
              {members.map(({ username, picture }) => (
                <div className={css.imageCont} key={`${id}-${username}`}>
                  <img src={picture} className={css.membImg} alt="user profile" />
                </div>
              ))}
            </div>
          </span>
        )}
        {skills?.length > 0 && (
          <div className="flex bewteen wrap">
            {skills.map(({ name }, index) => (
              <span className={css.skillCont} key={`${name}-${index}-${id}`}>
                <p className={css.skill}>{name}</p>
              </span>
            ))}
          </div>
        )}
      </div>
      <div className={css.chartsContainer}>
        {compensation?.data && (
          <div className={css.moneyCont}>
            <Money />
            <p className={css.currency}>{compensation.data.currency}</p>
            <p className={css.payment}>{`${convertToK(compensation.data.minAmount)} - ${convertToK(
              compensation.data.maxAmount,
            )}`}</p>
            <p className={css.period}>{parses.periodicity[compensation.data.periodicity]}</p>
          </div>
        )}
      </div>
    </div>
  )
}
