const form = ({ name, password, passwordConf, email, acceptButton }) => ({
  fieldsById: {
    name: {
      type: `text`,
      icon: 'person',
      ...name,
    },
    password: {
      type: `password`,
      icon: 'security',
      ...password,
    },
    password_confirmation: {
      type: `password`,
      icon: 'security',
      ...passwordConf,
    },
    email: {
      icon: 'email',
      type: `text`,
      ...email,
    },
  },
  sections: {
    fieldsIds: ['name', 'email', 'password', 'password_confirmation'],
  },
  submit: {
    acceptButton,
    styles: {
      justifyContent: 'flex-end',
      paddingTop: '20px',
    },
  },
})
export default form
