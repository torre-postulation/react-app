import React from 'react'
import css from './css.module.css'
import paper from 'assets/img/paper.png'
import { ReactComponent as Torre } from 'assets/icons/torre.svg'
import { useIntText, useMergedState } from 'hooks'
import formData from './form'
import { Form, Loader } from 'components'
import { Link } from 'react-router-dom'
import Request from 'api'
import { useDispatch } from 'react-redux'
import { setUser } from 'store/userActions'

export default function Login({ history: { push } }) {
  const { first, strong, link, form } = useIntText('register')
  const [{ loading, message }, setState] = useMergedState({ loading: false, message: null })
  const data = formData(form)
  const dispatch = useDispatch()

  function handleSubmit(data) {
    if (!loading) {
      setState(true)
      const request = new Request('register', { data })
      request
        .send()
        .finally(() => {
          setState(false)
        })
        .then(({ data: { user, token } }) => {
          localStorage.setItem('user', token)
          Request.authToken = token
          dispatch(setUser(user))
          push('/select-search')
        })
        .catch(({ validate: { email }, message }) => {
          setState({ message: email ? email : message })
        })
    }
  }

  return (
    <div className={`flex full ${css.wrapper}`}>
      <div className={`flex ${css.imageWrapper}`}>
        <img src={paper} alt="register" className={css.image} />
      </div>
      <div className={css.container}>
        <div className={`flex full ${css.logoCont}`}>
          <Torre className={css.torre} />
        </div>
        <span className={`flex full center`}>
          <h1 className={css.title}>{first}</h1>
          <h1 className={css.strong}>{strong}</h1>
        </span>
        <Form {...data} onSubmit={handleSubmit} />
        {loading && <Loader />}
        {message && <p className="full">{message}</p>}
        <Link to="/sign-up">
          <p className={css.link}>{link}</p>
        </Link>
      </div>
    </div>
  )
}
