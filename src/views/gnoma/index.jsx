import { Loader } from 'components'
import React, { Fragment, useEffect } from 'react'
import Request from 'api'
import css from './css.module.css'
import { useIntText, useMergedState } from 'hooks'
import { ReactComponent as Check } from 'assets/icons/check.svg'
import { ReactComponent as Globe } from 'assets/icons/country.svg'
import { ReactComponent as Remote } from 'assets/icons/signal.svg'
import { ReactComponent as Hand } from 'assets/icons/hand.svg'
import { ReactComponent as Contact } from 'assets/icons/contact.svg'
import { ReactComponent as Smily } from 'assets/icons/smily.svg'
import { ReactComponent as History } from 'assets/icons/history.svg'
import { ReactComponent as Book } from 'assets/icons/book.svg'
import LinkIcon from './LinkIcon'

function ExperienceCard({
  additionalInfo,
  fromMonth,
  fromYear,
  toMonth,
  toYear,
  name,
  organizations,
  remote,
  responsibilities,
}) {
  let responsText = ''
  if (responsibilities.length > 0) {
    responsibilities.forEach((resp, ind) =>
      ind > 0 ? (responsText = `${responsText}, ${resp}`) : (responsText = resp),
    )
  }

  return (
    <div className={`full flex ${css.experienceCard}`}>
      <div className={css.cardImage}>
        {organizations.length > 0 && <img src={organizations[0].picture} alt={name} />}
      </div>
      <div className={css.content}>
        <span className={css.nameCont}>
          <h2 className={css.nameCard}>{name}</h2>
          {remote && <Globe className={css.remoteInd} />}
        </span>
        {organizations.length > 0 && <h2 className={css.text}>{organizations[0].name}</h2>}
        {additionalInfo && <h2 className={css.text}>{additionalInfo}</h2>}
        {responsText && <h2 className={css.text}>{responsText}</h2>}
        {(fromMonth || toMonth) && (
          <span className="flex pad-top-10">
            <span className="flex">
              <p>{fromMonth}</p>
              <p className={css.year}>{fromYear}</p>
            </span>
            <p className={css.dash}>-</p>
            <span className="flex">
              <p>{toMonth}</p>
              <p className={css.year}>{toYear}</p>
            </span>
          </span>
        )}
      </div>
    </div>
  )
}

export default function Gnoma({
  match: {
    params: { username },
  },
}) {
  const [
    { loading, data: { person, strengths, interests, jobs, projects, education, languages } = {} },
    setState,
  ] = useMergedState({ loading: true })
  const {
    remoter,
    noRemoter,
    directory,
    skills,
    interest,
    experiences,
    languages: languagesLabel,
    jobs: jobsLabel,
    education: educationLabel,
    projects: projectsLabel,
  } = useIntText('torrePerson')

  function requestProfile() {
    const req = new Request('findPeople')
    req.send(username).then(({ data }) => setState({ data, loading: false }))
  }
  // eslint-disable-next-line
  useEffect(requestProfile, [])

  console.log(languages)
  return (
    <div className={css.wrapper}>
      <div className={`flex between wrap ${css.container}`}>
        {loading ? (
          <Loader />
        ) : (
          <Fragment>
            <div className={css.part}>
              <div className={`flex between ${css.paper}`}>
                <div className={css.imageCont}>
                  {person.picture && (
                    <img src={person.picture} alt="profile" className={css.picture} />
                  )}
                  {person.verified && (
                    <div className={css.verified}>
                      <Check className={css.verifiedIcon} />
                    </div>
                  )}
                </div>
                <div className={`${css.personalData}`}>
                  <h1 className={`full ${css.name}`}>{person.name}</h1>
                  <h1 className={`full ${css.role}`}>{person.professionalHeadline}</h1>
                  <h1 className={`full ${css.descrp}`}>{person.summaryOfBio}</h1>
                  <h1 className={`full ${css.user}`}>{`@${person.publicId}`}</h1>
                </div>
              </div>
              <div className="full pad-top-20">
                {person.location?.country && (
                  <div className={`flex center ${css.titleCont}`}>
                    <Globe className={css.strongIcon} />
                    <p className={css.sectTitle}>{person.location?.country}</p>
                  </div>
                )}
                <div className={`flex center ${css.titleCont}`}>
                  <Remote className={css.strongIcon} />
                  <p className={css.sectTitle}>{person.flags.remoter ? remoter : noRemoter}</p>
                </div>
                {person.links.length > 0 && (
                  <div className="full">
                    <div className={`flex center wrap ${css.titleCont}`}>
                      <Contact className={css.strongIcon} />
                      <p className={css.sectTitle}>{directory}</p>
                      <div className={css.line} />
                    </div>
                    <div className="full flex center pad-top-20">
                      {person.links.map(({ id, ...rest }) => (
                        <LinkIcon key={id} {...rest} />
                      ))}
                    </div>
                  </div>
                )}
                {strengths.length > 0 && (
                  <div className="full">
                    <div className={`flex center wrap ${css.titleCont}`}>
                      <Hand className={css.strongIcon} />
                      <p className={css.sectTitle}>{skills}</p>
                      <div className={css.line} />
                    </div>
                    <div className="pad-top-20">
                      <div className={`full flex wrap ${css.scrollSection}`}>
                        {strengths.map(({ id, name }) => (
                          <div className={css.chipsToMap} key={id}>
                            {name}
                          </div>
                        ))}
                      </div>
                    </div>
                  </div>
                )}
                {interests.length > 0 && (
                  <div className="full">
                    <div className={`flex center wrap ${css.titleCont}`}>
                      <Smily className={css.strongIcon} />
                      <p className={css.sectTitle}>{interest}</p>
                      <div className={css.line} />
                    </div>
                    <div className="pad-top-20">
                      <div className={`full flex wrap ${css.scrollSection}`}>
                        {interests.map(({ id, name }) => (
                          <div className={css.chipsToMap} key={id}>
                            {name}
                          </div>
                        ))}
                      </div>
                    </div>
                  </div>
                )}
              </div>
            </div>
            <div className={css.part}>
              {(jobs.length > 0 || education.length > 0 || projects.length > 0) && (
                <div className="full">
                  <div className={`flex center wrap ${css.titleContExp}`}>
                    <History className={css.strongIcon} />
                    <p className={css.sectTitle}>{experiences}</p>
                    <div className={css.line} />
                  </div>
                  {jobs.length > 0 && (
                    <div className="pad-top-20">
                      <h2 className={css.innerExpTitle}>{jobsLabel}</h2>
                      {jobs.map(({ id, ...rest }) => (
                        <ExperienceCard key={id} {...rest} />
                      ))}
                    </div>
                  )}
                  {education.length > 0 && (
                    <div>
                      <h2 className={css.innerExpTitle}>{educationLabel}</h2>
                      {education.map(({ id, ...rest }) => (
                        <ExperienceCard key={id} {...rest} />
                      ))}
                    </div>
                  )}
                  {projects.length > 0 && (
                    <div>
                      <h2 className={css.innerExpTitle}>{projectsLabel}</h2>
                      {projects.map(({ id, ...rest }) => (
                        <ExperienceCard key={id} {...rest} />
                      ))}
                    </div>
                  )}
                </div>
              )}
              {languages.length > 0 && (
                <div className="full">
                  <div className={`flex center wrap ${css.titleCont}`}>
                    <Book className={css.strongIcon} />
                    <p className={css.sectTitle}>{languagesLabel}</p>
                    <div className={css.line} />
                  </div>
                  <div className="pad-top-20">
                    <div className={`full flex wrap ${css.scrollSection}`}>
                      {languages.map(({ code, language: name }) => (
                        <div className={css.chipsToMap} key={code}>
                          {name}
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              )}
            </div>
          </Fragment>
        )}
      </div>
    </div>
  )
}
