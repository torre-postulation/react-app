import React from 'react'
import css from './css.module.css'
import { ReactComponent as Github } from 'assets/icons/github.svg'
import { ReactComponent as GitLab } from 'assets/icons/gitlab.svg'
import { ReactComponent as LinkedId } from 'assets/icons/linkedin.svg'
import { ReactComponent as Web } from 'assets/icons/web.svg'

export default function LinkIcon({ address, name }) {
  function open() {
    window.open(address)
  }

  function render() {
    switch (name) {
      case 'github':
        return <Github className={css.linkIcon} onClick={open} />
      case 'gitlab':
        return <GitLab className={css.linkIcon} onClick={open} />
      case 'linkedin':
        return <LinkedId className={css.linkIcon} onClick={open} />
      default:
        return <Web className={css.linkIcon} onClick={open} />
    }
  }
  return render()
}
