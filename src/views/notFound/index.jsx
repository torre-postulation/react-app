import React from 'react'
import css from './styles.module.css'
import { useIntText } from 'hooks'

export default function NotFound() {
  const { text } = useIntText('notFound')
  return (
    <div className={`full flex center top ${css.wrapper}`}>
      <div className={`full flex ${css.content} center wrap`}>
        <p className={css.numb}>404</p>
        <p className={css.title}>{text}</p>
      </div>
    </div>
  )
}
