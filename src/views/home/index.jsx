import SearchBar from 'components/Search'
import React, { useState } from 'react'
import css from './styles.module.css'
import { useSelector } from 'react-redux'
import SelectSearch from 'components/SelectSearch'

export default function Home() {
  const { deafultSearch } = useSelector(({ siteReducer }) => siteReducer)
  const [selected, select] = useState(deafultSearch)
  return (
    <div className={`full flex ${css.wrapper} center`}>
      <div className={`flex center wrap ${css.container}`}>
        <span className="flex">
          <h1 className={css.title}>Welcome to</h1>
          <h1 className={`${css.green} ${css.title}`}>Torre</h1>
        </span>
        <div className={`flex center full ${css.SearchBar}`}>
          <SearchBar searchMode={selected} />
        </div>
        <SelectSearch value={selected} onChange={(mode) => select(mode)} />
      </div>
    </div>
  )
}
