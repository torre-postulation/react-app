import React from 'react'
import css from './css.module.css'
import { ReactComponent as Torre } from 'assets/icons/torre.svg'
import { useIntText, useMergedState } from 'hooks'
import { Loader } from 'components'
import { useDispatch } from 'react-redux'
import { changeAccentColor, setUtils } from 'store/siteActions'
import Request from 'api'
import ColorsSelect from 'components/ColorsSelect'

export default function ColorSelect({ history: { push } }) {
  const { first, strong, third, link } = useIntText('selectColor')
  const [{ color, loading }, setState] = useMergedState({ color: null, loading: false })
  const dispatch = useDispatch()

  function saveUtils() {
    if (!loading) {
      setState({ loading: true })
      const req = new Request('createProfile', {
        data: {
          strong_color: color,
          default_search_kind: localStorage.getItem('selectedKind')
            ? localStorage.getItem('selectedKind')
            : 'jobs',
        },
      })
      req
        .send()
        .finally(() => setState({ loading: false }))
        .then(({ data: { utils } }) => {
          dispatch(setUtils(utils))
          push('/')
        })
    }
  }

  function selectColor(hex) {
    if (hex !== color) {
      setState({ color: hex })
      dispatch(changeAccentColor(hex))
    }
  }

  return (
    <div className={css.wrapper}>
      <div className={css.container}>
        <div className={`flex center ${css.logoCont}`}>
          <Torre className={css.logo} />
        </div>
        <div className={`flex center full ${css.titleCont}`}>
          <h2 className={css.title}>{first}</h2>
          <h2 className={css.strong}>{strong}</h2>
          {third && <h2 className={css.third}>{strong}</h2>}
        </div>
        <ColorsSelect value={color} onChange={selectColor} />
        {color && (
          <div className={`flex full ${css.goForward}`} onClick={saveUtils}>
            {link}
          </div>
        )}
        {loading && <Loader />}
      </div>
    </div>
  )
}
