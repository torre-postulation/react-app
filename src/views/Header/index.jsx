import React, { useEffect, useState } from 'react'
import css from './styles.module.css'
import { ReactComponent as Torre } from 'assets/icons/torre.svg'
import { ReactComponent as People } from 'assets/icons/people.svg'
import { Link, useLocation } from 'react-router-dom'
import SearchBar from 'components/Search'

export default function Header() {
  const { pathname } = useLocation()
  const [isHome, setHome] = useState(pathname === '/')

  useEffect(() => {
    if (pathname === '/') {
      if (!isHome) {
        setHome(true)
      }
    } else setHome(false)
  }, [pathname, isHome])

  return (
    <div className={`full center flex`}>
      <div className={`full ${css.container} flex ${isHome ? 'center' : 'between'}`}>
        {!isHome && (
          <div className={`flex center ${css.barContainer}`}>
            <Link className={`flex center ${css.iconCont}`} to="/my-profile">
              <People className={css.peopleIcon} />
            </Link>
            <SearchBar headMode />
          </div>
        )}
        <Link to="/">
          <Torre className={`${isHome ? css.icon : ''} ${css.torre}`} />
        </Link>
      </div>
    </div>
  )
}
