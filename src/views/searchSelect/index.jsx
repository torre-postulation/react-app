import React, { useState } from 'react'
import css from './css.module.css'
import { ReactComponent as Torre } from 'assets/icons/torre.svg'
import { ReactComponent as People } from 'assets/icons/people.svg'
import { ReactComponent as Case } from 'assets/icons/case.svg'
import { useIntText } from 'hooks'

export default function SearchSelect({ history: { push } }) {
  const { first, strong, job, person, link } = useIntText('selectSearch')
  const [select, selectKind] = useState(null)

  function goto() {
    push('/select-color')
    localStorage.setItem('selectedKind', select)
  }

  return (
    <div className={css.wrapper}>
      <div className={css.container}>
        <div className={`flex center ${css.logoCont}`}>
          <Torre className={css.logo} />
        </div>
        <div className={`flex center full ${css.titleCont}`}>
          <h2 className={css.title}>{first}</h2>
          <h2 className={css.strong}>{strong}</h2>
        </div>
        <div className={`full flex between ${css.cardCont}`}>
          <div
            className={`flex center wrap ${css.card} ${select === 'person' && css.selected}`}
            onClick={() => selectKind('person')}
          >
            <p className={css.cardTitle}>{person}</p>
            <People />
          </div>
          <div
            className={`flex center wrap ${css.card} ${select === 'job' && css.selected}`}
            onClick={() => selectKind('job')}
          >
            <p className={css.cardTitle}>{job}</p>
            <Case />
          </div>
        </div>
        {select && (
          <div className={`flex full ${css.goForward}`} onClick={goto}>
            {link}
          </div>
        )}
      </div>
    </div>
  )
}
