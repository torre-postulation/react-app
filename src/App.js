import React, { Fragment, useEffect } from 'react'
import { activateListeners, changeTitle } from 'utils'
import Routes from 'routes'
import { useDispatch, useSelector } from 'react-redux'
import { changeRoutesGroup } from 'store/siteActions'
import { setUser } from 'store/userActions'
import Header from 'views/Header'
import Request from 'api'
import { setUtils } from 'store/siteActions'
import RoutesList from 'routes/routes'
import { useLocation } from 'react-router-dom'
import { useMergedState } from 'hooks'

// activate match media listeners
activateListeners()

export default function App() {
  const {
    userReducer: { user },
    siteReducer: { routeGroup },
  } = useSelector(({ userReducer, siteReducer }) => ({ userReducer, siteReducer }))
  const dispatcher = useDispatch()
  const [{ useLayout }, setState] = useMergedState({
    useLayout: true,
  })
  const { pathname } = useLocation()

  function detectUserType() {
    if (user) {
      dispatcher(changeRoutesGroup(user.role))
    }
    if (!user && routeGroup !== 'public') {
      dispatcher(changeRoutesGroup('public'))
    }
  }

  function checkLocalStorage() {
    if (localStorage.getItem('user')) {
      Request.authToken = localStorage.getItem('user')
      dispatcher(setUser({ role: 'user' }))
      const me = new Request('me')
      me.send().then(({ data: { utils, ...rest } }) => {
        dispatcher(setUser(rest))
        if (utils) {
          dispatcher(setUtils(utils))
        }
      })
    }
  }

  // eslint-disable-next-line
  useEffect(detectUserType, [user])
  // eslint-disable-next-line
  useEffect(checkLocalStorage, [])
  useEffect(() => {
    const routes = RoutesList[routeGroup]
    const matchedRoute = routes.find(({ path }) => path === pathname)
    if (matchedRoute) {
      const { title = undefined, useLayout: renderLayout = true } = matchedRoute
      if (renderLayout !== useLayout) {
        setState({ useLayout: renderLayout })
      }
      if (title) {
        changeTitle(title)
      }
    }
    // eslint-disable-next-line
  }, [pathname, routeGroup, setState, useLayout])

  return (
    <Fragment>
      {useLayout && <Header />}
      <Routes />
    </Fragment>
  )
}
