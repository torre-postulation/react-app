// eslint-disable-next-line
export default {
  findPeople: {
    method: `GET`,
    url: 'torre-user',
  },
  findJob: {
    url: `opportunities`,
    method: `GET`,
    baseURL: `https://torre.bio/api/`,
  },
  search: {
    method: `POST`,
    baseURL: `https://search.torre.co/`,
  },
  login: {
    method: 'POST',
    url: 'login',
  },
  register: {
    method: 'POST',
    url: 'sign-up',
  },
  logout: {
    method: 'POST',
    url: 'logout',
  },
  profile: {
    method: 'GET',
    url: '/my-utils',
  },
  createProfile: {
    method: 'POST',
    url: '/my-utils',
  },
  updateProfile: {
    method: 'PUT',
    url: '/my-utils',
  },
  me: {
    method: 'GET',
    url: '/me',
  },
  mySearchs: {
    method: 'GET',
    url: '/my-searchs',
  },
  searchTool: {
    method: 'GET',
    url: '/last-searchs',
  },
  newSearch: {
    method: 'POST',
    url: '/new',
  },
}
