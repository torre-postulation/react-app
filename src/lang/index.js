const login = {
  esp: {
    first: 'Iniciar',
    strong: 'Sesión',
    link: '¿No tienes una cuenta?, Crea una aquí',
    form: {
      password: {
        label: 'Contraseña',
        placeholder: '******',
        rules: {
          presence: 'La contraseña es obligatoria',
          length: {
            minimum: 8,
            maximun: 255,
            tooLong: 'La contraseña excede la longitud de 255 carácteres',
            tooShort: 'La contraseña debe tener al menos 8 carácteres',
          },
        },
      },
      email: {
        label: 'Correo Electrónico',
        placeholder: 'ejemplo@ejemplo.com',
        rules: {
          presence: 'El correo electrónico es obligatorio',
          length: {
            minimum: 4,
            maximun: 255,
            tooLong: 'La contraseña excede la longitud de 255 carácteres',
            tooShort: 'La contraseña debe tener al menos 4 carácteres',
          },
        },
      },
      acceptButton: {
        label: 'Iniciar Sesión',
        color: 'black',
      },
    },
  },
  eng: {
    first: 'Log',
    strong: 'In',
    link: "don't you have a user?, Create a new one here",
    form: {
      password: {
        label: 'Password',
        placeholder: '******',
        rules: {
          presence: true,
          length: {
            minimum: 8,
            maximun: 255,
          },
        },
      },
      email: {
        label: 'Email',
        placeholder: 'example@example.com',
        rules: {
          presence: true,
          length: {
            minimum: 4,
            maximun: 255,
          },
        },
      },
      acceptButton: {
        label: 'Log In',
        color: 'black',
      },
    },
  },
}
const register = {
  esp: {
    first: 'Crear',
    strong: 'Cuenta',
    link: '¿Ya Tienes una cuenta?, inicia Sesión',
    form: {
      name: {
        label: 'Nombre y Apellido',
        placeholder: 'Ariana Alfonzo',
        rules: {
          presence: 'El nombre y apellido es obligatorio',
          length: {
            minimum: 5,
            maximun: 255,
            tooLong: 'El nombre y apellido excede la longitud de 255 carácteres',
            tooShort: 'El nombre y apellido debe tener al menos 8 carácteres',
          },
        },
      },
      password: {
        label: 'Contraseña',
        placeholder: '******',
        rules: {
          presence: 'La contraseña es obligatoria',
          length: {
            minimum: 8,
            maximun: 255,
            tooLong: 'La contraseña excede la longitud de 255 carácteres',
            tooShort: 'La contraseña debe tener al menos 8 carácteres',
          },
        },
      },
      passwordConf: {
        label: 'Confirmar Contraseña',
        placeholder: '******',
        rules: {
          presence: 'La contraseña es obligatoria',
          equality: 'password',
          length: {
            minimum: 8,
            maximun: 255,
            tooLong: 'La contraseña excede la longitud de 255 carácteres',
            tooShort: 'La contraseña debe tener al menos 8 carácteres',
          },
        },
      },
      email: {
        label: 'Correo Electrónico',
        placeholder: 'ejemplo@ejemplo.com',
        rules: {
          presence: 'El correo electrónico es obligatorio',
          length: {
            minimum: 4,
            maximun: 255,
            tooLong: 'La contraseña excede la longitud de 255 carácteres',
            tooShort: 'La contraseña debe tener al menos 4 carácteres',
          },
        },
      },
      acceptButton: {
        label: 'Registrarse',
        color: 'black',
      },
    },
  },
  eng: {
    first: 'Sign',
    strong: 'Up',
    link: 'do you already have an account? Log in here!',
    form: {
      name: {
        label: 'Full Name',
        placeholder: 'Ariana Alfonzo',
        rules: {
          presence: true,
          length: {
            minimum: 5,
            maximun: 255,
          },
        },
      },
      password: {
        label: 'Password',
        placeholder: '******',
        rules: {
          presence: true,
          length: {
            minimum: 8,
            maximun: 255,
          },
        },
      },
      passwordConf: {
        label: 'Password Confirmation',
        equality: 'password',
        placeholder: '******',
        rules: {
          presence: true,
          length: {
            minimum: 8,
            maximun: 255,
          },
        },
      },
      email: {
        label: 'Email',
        placeholder: 'example@example.com',
        rules: {
          presence: true,
          length: {
            minimum: 4,
            maximun: 255,
          },
        },
      },
      acceptButton: {
        label: 'Sign In',
        color: 'black',
      },
    },
  },
}
const selectSearch = {
  esp: {
    first: '¿Qué',
    strong: 'Buscas?',
    job: 'Empleos',
    person: 'Personas',
    link: 'Avanzar >',
  },
  eng: {
    link: 'Next >',
    first: 'What are you',
    strong: 'Looking For?',
    job: 'Jobs',
    person: 'People',
  },
}
const selectColor = {
  esp: {
    first: '¿Cuál es tu',
    strong: 'Color',
    third: 'favorito?',
    link: 'Guardar >',
  },
  eng: {
    link: 'Save >',
    first: 'Select your favorite',
    strong: 'Color',
    job: 'Jobs',
    person: 'People',
  },
}
const profile = {
  esp: {
    personalData: {
      strong: 'Datos',
      second: 'Personales',
    },
    userPreferences: {
      strong: 'Preferencias',
      second: 'de Usuario',
    },
    searchs: {
      strong: 'Búsquedas',
      second: 'e historial',
    },
    preferencesForm: ({ utils }) => [
      {
        id: 'language',
        label: 'Lenguaje',
        icon: 'globe',
        value: utils.language,
      },
      {
        id: 'default_search_kind',
        icon: 'search',
        label: 'Tipo de Búsquedas',
        value: utils.default_search_kind,
      },
      {
        id: 'strong_color',
        label: 'Color de Tema',
        icon: 'colors',
        value: utils.strong_color,
      },
    ],
    dataForm: (data) => [
      {
        id: 'name',
        label: 'Nombre y Apellido',
        icon: 'person',
        value: data.name,
      },
      {
        id: 'email',
        label: 'Correo Electrónico',
        icon: 'email',
        value: data.email,
      },
      {
        id: 'password',
        label: 'Contraseña',
        icon: 'security',
        value: '************',
      },
    ],
    searchsForm: ({ lasts_searchs, person_search, jobs_search, searchs_count }) => [
      {
        id: 'topSearch',
        label: 'Búsquedas más recientes',
        icon: 'top',
        value: lasts_searchs,
      },
      {
        id: 'searchCount',
        label: 'Cantidad de Búsquedas',
        icon: 'count',
        value: { person_search, jobs_search, searchs_count },
      },
    ],
    save: 'Guardar Cambios',
    cancel: 'Descartar Cambios',
    searches: 'búsquedas',
    personSearch: 'búsquedas de personas',
    jobsSearch: 'búsquedas de empleos',
    topSearchMessage: 'No se han realizado búsquedas',
  },
  eng: {
    personalData: {
      strong: 'Personal',
      second: 'Data',
    },
    userPreferences: {
      strong: 'User',
      second: 'Preferences',
    },
    searchs: {
      strong: 'Searchs',
      second: 'History',
    },
    searches: 'Searchs',
    personSearch: 'People Searchs',
    jobsSearch: 'Jobs Searchs',
    topSearchMessage: 'No Searchs founded',
    save: 'Save Changes',
    cancel: 'Discard Changes',
    preferencesForm: ({ utils }) => [
      {
        id: 'language',
        label: 'Language',
        icon: 'globe',
        value: utils.language,
      },
      {
        id: 'default_search_kind',
        icon: 'search',
        label: 'Searchs Kind',
        value: utils.default_search_kind,
      },
      {
        id: 'strong_color',
        label: 'Color Theme',
        icon: 'colors',
        value: utils.strong_color,
      },
    ],
    dataForm: (data) => [
      {
        id: 'name',
        label: 'Full Name',
        icon: 'person',
        value: data.name,
      },
      {
        id: 'email',
        label: 'Email',
        icon: 'email',
        value: data.email,
      },
      {
        id: 'password',
        label: 'Password',
        icon: 'security',
      },
    ],
    searchsForm: ({ lasts_searchs, person_search, jobs_search, searchs_count }) => [
      {
        id: 'topSearch',
        label: 'Recent Searchs',
        icon: 'top',
        value: lasts_searchs,
      },
      {
        id: 'searchCount',
        label: 'Search Counts',
        icon: 'count',
        value: { person_search, jobs_search, searchs_count },
      },
    ],
  },
}
const notFound = {
  esp: {
    text: 'No se encontró lo que buscabas',
  },
  eng: {
    text: "Could not find the page you're looking",
  },
}
const torrePerson = {
  esp: {
    remoter: 'Trabajador Remoto',
    noRemoter: 'Trabajador Presencial',
    directory: 'Directorio',
    skills: 'Habilidades',
    interest: 'Intereses',
    experiences: 'Experiencias',
    languages: 'Idiomas',
    jobs: 'Trabajos',
    education: 'Educación',
    projects: 'Proyectos',
  },
  eng: {
    remoter: 'Remote Employee',
    noRemoter: 'No Remote Employee',
    directory: 'Directory',
    skills: 'Skills',
    interest: 'Interest',
    experiences: 'Experience',
    languages: 'Languages',
    jobs: 'Jobs',
    education: 'Educationn',
    projects: 'Projects',
  },
}
const lang = {
  torrePerson,
  login,
  register,
  selectSearch,
  notFound,
  selectColor,
  profile,
}

export default lang
